uses sysutils;
type
	link=^Node;
      Node=record
        next, prev:link;
        num:integer;
      end;
var
	f1, f2:text;
	bufferS, bufferE: link;
	anum: integer;
        astr: string;

function numinbuffer(anum:integer; var buffer:link):boolean;
var p:link; aresult:boolean;
begin
	numinbuffer:=false;
	aresult:=false;
	if buffer <> nil then begin
		p:=buffer;
		while (p <> nil) and not aresult do begin
			if p^.num = anum then begin
				aresult:=true;
                                p:=nil;
                          end
			else
				p:=p^.next;
		end;
	end;
	numinbuffer:=aresult;
end;

procedure addtobuffer(anum:integer; var bufferS,bufferE:link);
var tmp:link;
begin
	new(tmp);
	tmp^.num:=anum;
        tmp^.next:=nil;
	if bufferE <> nil then begin
		tmp^.prev:=bufferE;
		bufferE^.next:=tmp;
		bufferE:=tmp;
	end
	else{������� �����⢥��� �������}
        begin
		tmp^.prev:=nil;
		bufferS:=tmp;
		bufferE:=tmp;
	end
end;

function getfrombuffer(var bufferS,bufferE:link):integer;
var tmp:link;
begin
	if bufferS <> nil then begin
		getfrombuffer:=bufferS^.num;
		tmp:=bufferS;
		bufferS:=bufferS^.next;
		if bufferS <> nil then bufferS^.prev:=nil;
		dispose(tmp);
	end
	else getfrombuffer:=-1;
end;

begin
	assign(f1, 'numfile1.txt');
 	reset(f1);
	assign(f2, 'numfile2.txt');
	reset(f2);
	if IOresult = 0 then begin
		while not eof(f1) do
			begin
				readln(f1, astr);
                                anum:=strtoint(astr);
				if not numinbuffer(anum, bufferS) then
					addtobuffer(anum, bufferS, bufferE);
			end;
		while not eof(f2) do
			begin
				readln(f2, astr);
                                anum:=strtoint(astr);
				if not numinbuffer(anum, bufferS) then
					addtobuffer(anum, bufferS, bufferE);
			end
	end;
        WriteLn('Result:');
	repeat
		anum:=getfrombuffer(bufferS, bufferE);
		if anum <> -1 then WriteLn(anum);
	until anum = -1;
	ReadLn;
end.
