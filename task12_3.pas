uses sysutils;
type
	astrtype = record
		str:string;
		num:integer;
	end;
	link=^Node;
      Node=record
        next, prev:link;
        data:astrtype;
      end;
var
	f1, f2:text;
	bufferS, bufferE: link;
	i: integer;
    astr: string;
    curitem: astrtype;

function inbuffer(astr:string; var buffer:link):boolean;
var p:link; aresult:boolean;
begin
	inbuffer:=false;
	if buffer <> nil then begin
		p:=buffer;
		while (p <> nil) do begin
			if p^.data.str = astr then
				begin
					inbuffer:=true;
                    p:=nil;
				end
			else
				p:=p^.next;
		end;
	end;
end;

procedure addtobuffer(astr:string; strnum:integer; var bufferS,bufferE:link);
var tmp:link;
begin
	new(tmp);
	tmp^.data.str:=astr;
	tmp^.data.num:=strnum;
	tmp^.next:=nil;
	if bufferE <> nil then begin
		tmp^.prev:=bufferE;
		bufferE^.next:=tmp;
		bufferE:=tmp;
	end
	else
	 begin
		tmp^.prev:=nil;
		bufferS:=tmp;
		bufferE:=tmp;
	end
end;

function getfrombuffer(var bufferS,bufferE:link):astrtype;
var tmp:link;
	aresult: astrtype;
begin
	if bufferS <> nil then begin
		getfrombuffer:=bufferS^.data;
		tmp:=bufferS;
		bufferS:=bufferS^.next;
		if bufferS <> nil then bufferS^.prev:=nil;
		dispose(tmp);
	end
	else
		begin
			aresult.str:='';
			aresult.num:=-1;
			getfrombuffer:=aresult;
		end;
end;

begin
	assign(f1, 'input.txt');
 	reset(f1);
	assign(f2, 'output.txt');
	rewrite(f2);
	if IOresult = 0 then begin
		i:=1;
		while not eof(f1) do
			begin
				readln(f1, astr);
				if not inbuffer(astr, bufferS) then
					addtobuffer(astr, i, bufferS, bufferE);
				inc(i);
			end;
                close(f1);
	end;
        WriteLn('Result:');
	repeat
		curitem:=getfrombuffer(bufferS, bufferE);
		if curitem.num <> -1 then WriteLn(f2, curitem.num, ':', curitem.str);
	until curitem.num = -1;
        close(f2);
end.
