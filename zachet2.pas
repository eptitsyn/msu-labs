program task2;

const maxwordlength = 6;
      allowedchars = ['a'..'z', 'A'..'Z', ',', '.'];


type link=^Node;
      Node=record
        next:link;
        word:string;
      end;
      list = link;

//�㭪�� �ࠢ����� 2� ᫮�
//�᫨ word1 ����� word2�� � true
function wordcompare(word1, word2:string):boolean;
var i:integer;
begin
  i:=1;
  if (Length(word1) > 0) and (Length(word2)>0) then//�஢�ਬ �� ᫮�� �� �����
  begin
   while (word1[i]=word2[i]) and (i < maxwordlength)
      and (i < Length(word1)) and (i < Length(word2)) do
      //���� �㪢� � ᫮��� ᮢ������ � �� �⪭㫨�� � ����� ᫮�� ��� ��࠭��⥫� ���⨬ ���稪
    begin
      inc(i);
    end;
    if (word1[i] <= word2[i]) then //ᥩ�� �� �� �����
      begin
          if (i = Length(word1)) or (i = Length(word2)) then
          {�᫨ �� ��諨 �� 横�� �� �� ⮣� �� �⪭㫨�� � ����� ᫮��,
          � �஢�ਬ �� �� ���� �� ��ࢮ�� ᫮��?}
          begin
            if (Length(word1)<Length(word2)) then
              wordcompare:=true
            else
              wordcompare:=false;
          end
          else
           wordcompare:=true
      end
    else
      wordcompare:=false
  end

  else
  begin{���� �� ᫮� ���⮥}
      wordcompare:=false
  end;
end;


//���������� ᫮�� � ��䠢�⭮� ���浪�
procedure addword(var alist:list; word:string);
var p,newlink:link; breakflag:boolean;
begin
  if alist <> nil then
    begin
      if wordcompare(word,alist^.word) then {���� ��⠢��� 1� ������}
        begin
          new(p);
          p^.word:=word;
          p^.next:=alist;
          alist:=p;
        end
      else {2�  � �����}
        begin
          p:=alist;
          breakflag:=false;
          while (p^.next <> nil) and not breakflag do
              if not wordcompare(word,p^.next^.word) then
                p:=p^.next
              else breakflag:=true;
          new(newlink);
          newlink^.word:=word;
          newlink^.next:=p^.next;
          p^.next:=newlink;
        end;
    end
  else
    begin{��⠢��� �����⢥��� ������⮬ ᯨ᪠}
      new(p);
      p^.word:=word;
      p^.next:=nil;
      alist:=p;
    end;
end;

//��楤�� ���뢠���� ᫮�� �� ��᫥����⥫쭮��
//�� �� ������ ���� ��楤�� ᮣ��᭮ �.2.2.1
procedure getnextword (var word:string; var nextchar:char; var error:boolean);
begin
  error:=false;
  word:='';
  //�������� �஢��� �� �宦����� � AZ � ������
  repeat
      read(nextchar);
      if (Length(word) >= 6) and not((nextchar=',') or (nextchar='.')) then
      begin
          WriteLn('������ �室�� �����, ᫮�� ������� 6 ᨬ�����');
          error:=true;
      end;
      if not (nextchar in allowedchars) then
      begin
          WriteLn('������ �室�� �����, �������⨬� ᨬ��� ', nextchar);
          error:=true;
      end;
      if not (error or (nextchar=',') or (nextchar='.'))
       then begin
        word:=word+nextchar;
      end
  until (nextchar=',') or (nextchar='.') or error;
end;

var
  a:char;
  word:string;
  alist,p: list;//㪠��⥫� �� ��砫� ᯨ᪠
  error:boolean;
begin
  error:=false;
  Write('������ �����:');
  repeat
    getnextword(word,a, error);
    if not error then addword(alist, word);
  until (a = '.') or error;
  if not error then
  WriteLn('���᮪ ᫮�');
  begin
    p:=alist;
    while p <> nil do
    begin
      WriteLn(p^.word);
      p:=p^.next;
    end;
    Readln;
  end;
  Readln;
  Readln;
end.
