type
	link=^Node;
      Node=record
        next:link;
        str:string;
      end;

procedure push(astr:string; var stack:link);
var tmp:link;
begin
	new(tmp);
	tmp^.str:=astr;
	if stack = nil then begin
          tmp^.next:=nil;
	  stack:= tmp
        end
	else
	  begin
	    tmp^.next:=stack;
	    stack:=tmp;
	  end
end;

function get(var stack:link):string;
var tmp:link;
begin
	if stack <> nil then
	  begin
	    get:=stack^.str;
	    tmp:=stack;
	    stack:=stack^.next;
	    dispose(tmp);
	  end
       else
	get:='';
end;

function strhasnum(astr:string):boolean;
var i:integer; resultat:boolean;
begin
 resultat:=false;
 i:=1;
 while (i < length(astr)) and not resultat do
  begin
   if astr[i] in ['0'..'9'] then resultat:=true;
   inc(i);
  end;
 strhasnum:=resultat;
end;

var
	f1:text;
	curstr:string;
        stack:link;
begin
  assign(f1, 'text.txt');
  reset(f1);
  if IOresult = 0 then begin
  while not eof(f1) do
	begin
	 readln(f1,curstr);
	 if strhasnum(curstr) then push(curstr,stack);
	end;
  WriteLn('�������:');
  while (stack <> nil) do
	WriteLn(get(stack))
 end;
 ReadLn
end.
