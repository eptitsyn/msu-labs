program task1;

const
    eps = 0.001;

type
    TF = function (x:real):real;

function FminusG(f,g:TF; x:real):real;
begin
  FminusG:=f(x)-g(x);
end;

  //��楤�� ���᫥��� ���� ᮣ��᭮ �.1.3.2 �ॡ������ � �ணࠬ��
  //f,g - �㭪樨; f1, g1 - �ந������ �㭪権;
  //a,b - ����� ��१�� ��� ���᪠, eps1 - �筮��� ���᫥���
procedure root(f, g, f1, g1:TF; a, b, eps1:real; var x:real);
  var c, d:real;{d- Xn, c- Xn+1}
  vozrast, vishe:boolean;
begin
  vozrast:= (f(a)-g(a)) < 0; {�����⠥�}
  vishe:= FminusG(f,g,((a+b)/2)) > (FminusG(f,g,a)+FminusG(f,g,b)) / 2;{���}
 if (vozrast and not vishe) or (not vozrast and vishe) then {��砩 1}  c:=b
   else {��砩 2} c:=a;
 repeat
    d:=c;
    //WriteLn(c);
    c:=d - FminusG(f,g,d) / FminusG(f1, g1, d)
 until (abs(c-d) < eps1);
  x:=c;
end;

//�㭪�� ���᫥��� ��⥣ࠫ� ��⮤�� ��אַ㣮�쭨��� �� N ࠧ������
function integralrectangle(f:TF; a,b:real; n:integer):real;
var i:integer;
  sum, h:real;
begin
sum:=0;
h:=(b-a)/n;
  for i := 0 to n-1 do begin
    sum:=sum+f(a+(i+0.5)*h);
  end;
  integralrectangle:=sum*h;
end;

//�㭪�� ���᫥��� ��⥣ࠫ� ᮣ��᭮ �.1.3.3 �ॡ������ � �ணࠬ��
function integral(f:TF; a,b,eps2:real):real;
const
  n0 = 10;
  p = 1/3;
var
  n:integer;
  i1, i2:real;
begin
  n:=n0;
  i2:=integralrectangle(f,a,b,n);
  repeat
    i1:=i2;
    //writeln(n);
    n:=n*2;
    i2:=integralrectangle(f,a,b,n);
  until (p*abs(i1-i2)) < eps2;
  integral:=i2;
end;

{$F+}
   function f1 (x:real):real; begin f1:=exp(-x)+3 end;
   function f2 (x:real):real; begin f2:=2*x-2 end;
   function f3 (x:real):real; begin f3:=1/x end;
   //�㭪樨 ����� �ந�������
   function Df1 (x:real):real; begin Df1:= -exp(-x) end;
   function Df2 (x:real):real; begin Df2:=2 end;
   function Df3 (x:real):real; begin Df3:=-(1/sqr(x)) end;
  //�㭪樨 ����� �ந�������
   function DDf1 (x:real):real; begin DDf1:= exp(-x) end;
   function DDf2 (x:real):real; begin DDf2:=0 end;
   function DDf3 (x:real):real; begin DDf3:=( 2 / (x * x * x) ) end;

var
   root1, root2, root3:real;
   x, area:real;

begin
  root(@f1,@f2,@Df1,@Df2, 1.0, 4.0, eps, X);{a � b ᮣ��᭮ �.1.1 ��।��塞 ������}
  root1:=x;
  WriteLn('f1 ���ᥪ��� f2 � �窥 ',x:3:5);
  root(@f1,@f3,@Df1,@Df3, 0.1, 4.0, eps, X);{a � b ᮣ��᭮ �.1.1 ��।��塞 ������}
  root2:=x;
  WriteLn('f1 ���ᥪ��� f3 � �窥 ',x:3:5);
  root(@f2,@f3,@Df2,@Df3, 0.1, 4.0, eps, X);{a � b ᮣ��᭮ �.1.1 ��।��塞 ������}
  root3:=x;
  WriteLn('f2 ���ᥪ��� f3 � �窥 ',x:3:5);

  area:= integral(@f1, root2, root1, eps) -
          integral(@f2, root3, root1, eps) -
          integral(@f3, root2, root3, eps);
  Writeln('���頤� 䨣���: ', area:3:7);
  readln;
end.
