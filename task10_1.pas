type complex = record
  re, im:real;
end;

function complexsum(a,b:complex):complex;
var aresult:complex;
begin
  aresult.re:=a.re+b.re;
  aresult.im:=a.im+b.im;
  complexsum:=aresult;
end;

function complexminus(a,b:complex):complex;
var aresult:complex;
begin
  aresult.re:=a.re-b.re;
  aresult.im:=a.im-b.im;
  complexminus:=aresult;
end;

function complexmult(a,b:complex):complex;
var aresult:complex;
begin
  aresult.re:=a.re*b.re-a.im*b.im;
  aresult.im:=(a.re*b.im+a.im*b.re);
  complexmult:=aresult;
end;

function complexdiv(a,b:complex):complex;
var aresult:complex;
begin
  aresult.re:=(a.re*b.re+a.im*b.im)/(sqr(b.re)+sqr(b.im));
  aresult.im:=(b.re*a.im-a.re*b.im)/(sqr(b.re)+sqr(b.im));
  complexdiv:=aresult;
end;

var a, b, c, d, e, f, res:complex;
begin
a.re:=17; a.im:=31;
b.re:=7; b.im:=1;
c.re:=12; c.im:=0;
d.re:=1; d.im:=-1;
e.re:=0.25; e.im:=0;
f.re:=0; f.im:=1;

res:=complexsum(complexdiv(a,b),complexdiv(c,complexmult(d,complexmult(d,complexmult(d,d)))));
res:=complexsum(complexmult(res, e), f);

WriteLn('�������:', res.re:5:0,'+',res.im:5:0,'i');

end.

